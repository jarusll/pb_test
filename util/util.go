package util

import (
	"encoding/json"

	"github.com/pocketbase/pocketbase"
	"github.com/pocketbase/pocketbase/forms"
	"github.com/pocketbase/pocketbase/models"
	"github.com/pocketbase/pocketbase/models/schema"
	"github.com/pocketbase/pocketbase/tools/types"
)

func StrToJson(str string) map[string]interface{} {
	var token map[string]interface{}
	json.Unmarshal([]byte(str), &token)
	return token
}

func MakeGameCollection(app *pocketbase.PocketBase) (bool, error) {
	collection := &models.Collection{}

	_, err := app.Dao().FindCollectionByNameOrId("Game")

	if err == nil {
		return true, nil
	}

	form := forms.NewCollectionUpsert(app, collection)
	form.Name = "Game"
	form.Type = models.CollectionTypeBase
	form.ListRule = nil
	form.ViewRule = nil
	authUserOnly := "@request.auth.id != ''"
	form.CreateRule = types.Pointer(authUserOnly)
	form.UpdateRule = types.Pointer(authUserOnly)
	form.DeleteRule = types.Pointer(authUserOnly)
	form.Schema.AddField(&schema.SchemaField{
		Name:     "name",
		Type:     schema.FieldTypeText,
		Required: true,
		Unique:   true,
		Options: &schema.TextOptions{
			Max: types.Pointer(10),
		},
	})
	form.Schema.AddField(&schema.SchemaField{
		Name: "description",
		Type: schema.FieldTypeText,
		Options: &schema.TextOptions{
			Max: types.Pointer(300),
		},
	})
	form.Schema.AddField(&schema.SchemaField{
		Name: "image",
		Type: schema.FieldTypeFile,
		Options: &schema.FileOptions{
			MaxSelect: 1,
			MaxSize:   1000000,
		},
	})

	if err := form.Submit(); err != nil {
		return false, err
	}
	return false, nil
}
