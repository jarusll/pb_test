package client

import (
	"log"
	"os"

	"github.com/joho/godotenv"
	"gopkg.in/h2non/gentleman.v2"
	"gopkg.in/h2non/gentleman.v2/plugins/query"
)

// make a struct

func Auth() string {
	godotenv.Load()

	client := gentleman.New()
	client.URL("https://id.twitch.tv/oauth2/token")
	setQueryParams(client)

	res, err := client.Post().Send()
	if err != nil {
		log.Fatal(err)
	}

	return res.String()
}

func setQueryParams(client *gentleman.Client) {
	clientId := os.Getenv("CLIENT_ID")
	clientSecret := os.Getenv("CLIENT_SECRET")
	grantType := "client_credentials"
	client.Use(query.Set("client_id", clientId))
	client.Use(query.Set("client_secret", clientSecret))
	client.Use(query.Set("grant_type", grantType))
}
