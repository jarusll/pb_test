package main

import (
	"log"
	"pb_game/game/util"

	"github.com/pocketbase/pocketbase"
	"github.com/pocketbase/pocketbase/core"
)

func main() {
	app := pocketbase.New()

	// create all the collections after app is bootstrapped
	app.OnAfterBootstrap().Add(func(e *core.BootstrapEvent) error {
		log.Println(e.App)
		_, gameCollectionError := util.MakeGameCollection(app)
		if gameCollectionError != nil {
			log.Fatal(gameCollectionError)
		}
		return nil
	})
	if err := app.Start(); err != nil {
		log.Fatal(err)
	}
}
