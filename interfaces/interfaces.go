package interfaces

type Client interface {
	// if the service requires auth
	Auth() string

	// search
	Search(query string) []Record
}

type Record interface {
	// name
	Name() string

	// description
	Description() string

	// image
	Banner() string
}
